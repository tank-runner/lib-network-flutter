import 'package:lib_base/data_repository/data_repository.dart';
import 'package:lib_network/models/service_address_model.dart';

class ServiceAddressRepository extends DataRepository<ServiceAddressModel> {
  static const String kId = "ServiceAddressRepository";

  @override
  String get id => ServiceAddressRepository.kId;

  static final ServiceAddressRepository instance =
      ServiceAddressRepository._internal();

  ServiceAddressRepository._internal();

  @override
  dynamic encode(ServiceAddressModel data) {
    return data.toJson();
  }

  @override
  ServiceAddressModel? decode(dynamic data) {
    return data != null ? ServiceAddressModel.fromJson(Map.from(data)) : null;
  }
}
