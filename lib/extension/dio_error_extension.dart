import 'package:dio/dio.dart';

extension DioErrorExtension on DioException {
  String get localizedMessage {
    switch (type) {
      case DioExceptionType.connectionTimeout:
      case DioExceptionType.receiveTimeout:
      case DioExceptionType.sendTimeout:
        return "网络请求超时，请稍后重试。";

      case DioExceptionType.cancel:
        return "请求已取消请重试";

      case DioExceptionType.badCertificate:
      case DioExceptionType.badResponse:
      case DioExceptionType.connectionError:
      case DioExceptionType.unknown:
        return "网络请求失败，请稍后重试。";
    }
  }
}
