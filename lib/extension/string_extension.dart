import 'package:lib_network/http_service.dart';

extension AddressExtension on String {
  Future<String?> get baseH5Url async {
    return HttpService.instance.h5BaseUrl;
  }

  Future<String?> get absoluteH5Url async {
    return absoluteUrl(await baseH5Url);
  }

  String absoluteUrl(String? baseUrl) {
    if (baseUrl == null) {
      return this;
    }

    Uri uri = Uri.parse(this);

    if (uri.host.isNotEmpty) {
      return this;
    } else if (startsWith("/")) {
      return "$baseUrl$this";
    } else {
      return "$baseUrl/$this";
    }
  }
}
