import 'package:flutter/services.dart';
import 'package:lib_network/http_service.dart';

abstract class HttpServiceDataSource {
  Future<String?> get apiBaseUrl;

  Future<Map<String, String>?> getAccessToken(HttpService httpService);

  Future<Map<String, String>?> getHeaders(HttpService httpService);

  Future<bool> refreshToken();

  Future<bool> invalidToken({String? message});

  Future<List<ByteData>?> secCertificates();
}
