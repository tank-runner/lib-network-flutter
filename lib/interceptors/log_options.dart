// ignore_for_file: unused_element, unused_field

import 'package:lib_base/option_set/generator/annotation.dart';
import 'package:lib_base/option_set/option_set.dart';

part 'log_options.g.dart';

@option_set
enum _LogOptions {
  requestMethod,
  requestHeaders,
  requestBody,
  formatRequestAscURL,
  responseHeaders,
  responseBody,
  error;
}
