// GENERATED CODE - DO NOT MODIFY BY HAND

// ignore_for_file: lint_a, lint_b

part of 'log_options.dart';

// **************************************************************************
// OptionSetGenerator
// **************************************************************************

class LogOptions extends OptionSet<LogOptions> {
  const LogOptions._(int rawValue) : super(rawValue);

  static const requestMethod = LogOptions._(1 << 0);

  static const requestHeaders = LogOptions._(1 << 1);

  static const requestBody = LogOptions._(1 << 2);

  static const formatRequestAscURL = LogOptions._(1 << 3);

  static const responseHeaders = LogOptions._(1 << 4);

  static const responseBody = LogOptions._(1 << 5);

  static const error = LogOptions._(1 << 6);

  @override
  List<String> get optionNames => const [
        'requestMethod',
        'requestHeaders',
        'requestBody',
        'formatRequestAscURL',
        'responseHeaders',
        'responseBody',
        'error'
      ];
  @override
  LogOptions initWithRawValue(int rawValue) {
    return LogOptions._(rawValue);
  }
}
